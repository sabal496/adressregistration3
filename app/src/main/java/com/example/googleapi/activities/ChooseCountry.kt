package com.example.googleapi.activities

import android.R.attr.y
import android.app.Activity
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.googleapi.Models.CountryesModel
import com.example.googleapi.R
import com.example.googleapi.adapters.CountriesAdapter
import kotlinx.android.synthetic.main.activity_choose_country.*
import kotlinx.android.synthetic.main.main_toolbar.*


class ChooseCountry : AppCompatActivity() {
   lateinit var adapter:CountriesAdapter
    var countries= mutableListOf<CountryesModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_country)
        init()
    }
    private fun init(){
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon!!.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        done.text="Done"

        countries.addAll(intent.extras?.getParcelableArrayList("countries")!!)
        var selected=intent.extras?.getInt("selected",0)
        adapter= CountriesAdapter(countries)
        if (selected != null) {
            adapter.selected=selected
        }

        countriesrecuclerview.isNestedScrollingEnabled = false
        countriesrecuclerview.layoutManager=LinearLayoutManager(this)
        countriesrecuclerview.adapter=adapter


        cancelbtn.setOnClickListener(){
            onBackPressed()
        }

        done.setOnClickListener(){
          done()
            onBackPressed()
        }

    }

    private fun done(){
        var intent=intent
        intent.putExtra("position",adapter.selected)
        setResult(Activity.RESULT_OK,intent)
    }
    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==android.R.id.home){
            onBackPressed()
        }
        return true
    }
}
