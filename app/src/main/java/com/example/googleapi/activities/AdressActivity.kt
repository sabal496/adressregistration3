package com.example.googleapi.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Parcelable
import android.view.MenuItem
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.googleapi.Models.CountryesModel
import com.example.googleapi.R
import com.example.googleapi.dataloaders.ApiRequest2
import com.example.googleapi.interfaces.CallbackApi
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_adress.*
import kotlinx.android.synthetic.main.custom_chooser.view.*
import kotlinx.android.synthetic.main.dialog_vwindow.*
import kotlinx.android.synthetic.main.main_toolbar.*
import org.json.JSONObject


class AdressActivity : AppCompatActivity() {
    var mylist= mutableListOf<CountryesModel>()
    var randompos=0
    companion object{
        var REQUEST_COUNTRIES=1
        var REQUEST_ADRESSES=2
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adress)
        init()
    }
    private fun init(){
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon!!.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        done.text="Finalize"

        street.tiitlechoser.text="Street Adress"

        ApiRequest2.getRequest(ApiRequest2.METHOD,object :CallbackApi{
            override fun onResponse(value: String?) {
                    var json=JSONObject(value)
                    var jsonarray=  json.getJSONArray("results")
                    mylist=  Gson().fromJson(jsonarray.toString(),Array<CountryesModel>::class.java).toMutableList()
                randompos=(0 until mylist.size).random()
                street.setOnClickListener(){
                    getadress(randompos)
                }

                var model=mylist[randompos]
                country.chooser.text=model.name
                country.setOnClickListener(){
                    openCountriesActivity(randompos)
                }
            }
            override fun onFailure(value: String?) {

             }
        },this)

        nextbtn.setOnClickListener(){
            dialog()
        }
    }
    private fun dialog(){
        val dialog=Dialog(this)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_vwindow)

        val params:ViewGroup.LayoutParams=dialog.window!!.attributes
        params.width=ViewGroup.LayoutParams.MATCH_PARENT
        params.height=ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes=params as WindowManager.LayoutParams
        dialog.dialogbtn.setOnClickListener(){
            dialog.dismiss()
        }
        dialog.show()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==android.R.id.home){
            super.onBackPressed()
        }
        return true
    }

    private fun openCountriesActivity( position:Int){
        var intent=Intent(this,ChooseCountry::class.java)
        intent.putParcelableArrayListExtra("countries",mylist as java.util.ArrayList<out Parcelable>)
        intent.putExtra("selected",position)
        startActivityForResult(intent,REQUEST_COUNTRIES)
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)
    }

    private fun getadress(position:Int){
        val intet= Intent(this,
            ChooseAdressActivity::class.java)
        intet.putExtra("countryid",mylist[randompos].iso2)
        startActivityForResult(intet, REQUEST_ADRESSES)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode==Activity.RESULT_OK){
             if(requestCode==REQUEST_COUNTRIES){
                 var selectedpos= data?.extras?.getInt("position",0)
                 var model=mylist[selectedpos!!]
                 country.chooser.text=model.name
                 randompos=selectedpos
             }
            else if(requestCode== REQUEST_ADRESSES){
                 var adress= data?.extras?.getString("adress","")
                 val hashMap =
                     data?.getSerializableExtra("details") as HashMap<String, String>
                 street.chooser.text=adress
                 city.setText(hashMap["city"])
                 state.setText(hashMap["county"])
                 zipcode.setText(hashMap["zipcode"])

             }
         }
    }


}
