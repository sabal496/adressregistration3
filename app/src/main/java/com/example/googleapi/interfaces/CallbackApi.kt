package com.example.googleapi.interfaces

interface CallbackApi {
    fun onResponse(value:String?)
    fun onFailure(value:String?)
}