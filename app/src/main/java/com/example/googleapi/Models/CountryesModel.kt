package com.example.googleapi.Models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class CountryesModel(var name:String,var iso2:String):Parcelable {
}
